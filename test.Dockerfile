FROM lliccien/nginx-php:drupal as builder

RUN rm *.*

COPY composer.json .

COPY composer.lock .

RUN composer install

FROM lliccien/nginx-php:env as unittest

WORKDIR /var/www/html

RUN rm index.html index.nginx-debian.html

COPY . .

COPY --from=builder /var/www/html .

RUN composer run-script drupal-phpunit-upgrade

CMD vendor/bin/phpunit -c ./core/phpunit.xml modules/custom --filter "/Unit|Kernel/" --coverage-html ./coverage/unit
