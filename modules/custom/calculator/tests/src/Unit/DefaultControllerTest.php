<?php


namespace Drupal\Tests\calculator\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\calculator\Controller\DefaultController;


/**
 * @coversDefaultClass \Drupal\calculator\Controller\DefaultController
 * @package Drupal\Tests\calculator\Unit\DefaultControllerTest
 * @group Calculator
 */
class DefaultControllerTest extends UnitTestCase{


  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * @dataProvider additionProvider
   *
   * @param $a
   * @param $b
   * @param $expected
   */
  public function testAdd($a, $b, $expected)
  {
    $default = new DefaultController();

    $result = $default->add($a, $b);

    $this->assertEquals($expected, $result, "Error in Add");
  }

  /**
   * @codeCoverageIgnore
   */
  public function additionProvider()
  {
    return [
      [0, 1, 1],
      [1, 0, 1],
      [1, 1, 2]
    ];
  }


}
