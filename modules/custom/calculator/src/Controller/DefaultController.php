<?php

namespace Drupal\calculator\Controller;

use Drupal\Core\Controller\ControllerBase;


/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * @param $a
   * @param $b
   *
   * @return mixed
   */
  function add($a, $b) {
    return $a + $b;
  }

}
