FROM node:lts


RUN apt-get update && apt-get install xvfb libgtk2.0-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 -y

RUN npm install npx

RUN mkdir /app

WORKDIR /app

#COPY package.json .
#COPY package-lock.json .

#RUN npm install

#CMD ls -sl

CMD npm ci && \
    npm run cy:report
#& wait-on http://drupal
