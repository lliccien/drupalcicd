# language: es
Característica: Inicio de sesión

  Como Usuario
  Quiero iniciar sesión desde la pagina principal
  Para ver el contenido esclusivo para usuarios

  Escenario: Boton de inicio de session
    Dado que estoy en la pagina pincipal
    Entonces puedo ver un enlace a  "Iniciar sesión"

  Escenario: Pulsar el boton de inicio de sesión
    Dado Pulso el boton de inicio de sesión
    Entonces me lleva la pagina de inico de session
    Y puedo ver el Titulo "Iniciar sesión"
    Y puedo ver el formulario de iniciar sesión

  Escenario: Rellenar formularo e iniciar sesión con un usurio existente
    Dado que estoy en la pagina de inicias sesión
    Y escribo mi ususrio y mi contraseña en el formulario
    Y pulso el boton "Iniciar sesión"
    Entonces me lleva a la pagina de mi cuenta de usuario
    Y puedo ver el titulo con mi nombre de usuario


