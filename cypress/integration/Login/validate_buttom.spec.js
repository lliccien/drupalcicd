import {Given, Then} from "cypress-cucumber-preprocessor/steps";


Given('que estoy en la pagina pincipal', function () {
  cy.visit('/');
});

Then('puedo ver un enlace a  {string}', function (string) {
  cy.get('#block-bartik-account-menu > .content > .clearfix > .menu-item > a').should('have.text', string);
  cy.wait(3000);
  cy.screenshot('validate_buttom');
});
