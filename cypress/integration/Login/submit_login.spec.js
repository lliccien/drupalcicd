import {Given, Then} from "cypress-cucumber-preprocessor/steps";

Given('que estoy en la pagina de inicias sesión', function () {
  cy.visit('/user/login');
});

Given('escribo mi ususrio y mi contraseña en el formulario', function () {
  cy.get('#edit-name').type('admin');
  cy.get('#edit-pass').type('123456');
  cy.wait(3000);
  cy.screenshot('submit_login1', {blackout: ['#edit-name']});
});


Given('pulso el boton {string}', function (string) {
  cy.get('#user-login-form > #edit-actions > #edit-submit').click();
});

Then('me lleva a la pagina de mi cuenta de usuario', function () {
  cy.url().should('include', '/user')
});

Then('puedo ver el titulo con mi nombre de usuario', function () {
  cy.get('.js-quickedit-page-title').should('contain', 'admin');
  cy.wait(3000);
  cy.screenshot('submit_login2');
});

