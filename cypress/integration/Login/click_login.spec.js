import {Given, Then} from "cypress-cucumber-preprocessor/steps";

Given('Pulso el boton de inicio de sesión', function () {
  cy.get('#block-bartik-account-menu > .content > .clearfix > .menu-item > a').click();
});

Then('me lleva la pagina de inico de session', function () {
  cy.url().should('include', 'user/login')
});

Then('puedo ver el Titulo {string}', function (string) {
  cy.get('.title').should('contain', string);
});


Then('puedo ver el formulario de iniciar sesión', function () {
  cy.get("form.user-login-form").should('be.visible');
  cy.wait(3000);
  cy.screenshot('click_login');
});
