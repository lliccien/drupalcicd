import { Given, Then } from 'cypress-cucumber-preprocessor/steps';

Given('que estoy en la pagina http:\/\/localhost\/', function () {
  cy.visit('/');
});

Then('quiero ver el titilo de la pagina', function () {
  // Write code here that turns the phrase above into concrete actions
  cy.get('.title').should('contain', 'Drupal CI/CD Gitlab');
  cy.wait(3000);
  cy.screenshot('home');
});
